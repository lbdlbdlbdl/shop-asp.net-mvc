﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace Shop.WebUI.Helpers
{
    public class HashHelper
    {
        public static byte[] GetHash(string password)
        {
            HashAlgorithm hash = new SHA256Managed();
            byte[] plainTextBytes = System.Text.Encoding.UTF8.GetBytes(password);
            return hash.ComputeHash(plainTextBytes);
        }
    }
}