﻿using Shop.Domain.Entities;
using Shop.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.WebUI.Helpers
{
    public static class ToViewModelHelper
    {
        public static PrimaryAttributesViewModel ToPrimaryAttributesViewModel(this Product model)
        {
            return new PrimaryAttributesViewModel()
            {
                Id = model.Id,
                Name = model.Name,
                Image = model.ProductImage,
                CategoryId = model.CategoryId,
                Description = model.Description,
                Price = model.Price,
                Brand = model.Brand.Name
            };
        }
    }
}