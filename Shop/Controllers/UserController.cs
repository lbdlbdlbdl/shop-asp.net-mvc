﻿using Shop.WebUI.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Shop.Services.Users;
using Shop.Services.Users.Interfaces;
using Shop.Services.ShoppingCarts;
using Shop.Services.Authentification;
using Shop.Core.Work;

namespace Shop.WebUI.Controllers
{
    public class UserController : Controller
    {
        #region Fields

        private readonly IUserRegistrationService _userRegistrationService;
        private readonly IUserService _userService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IAuthentificationService _authService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor;

        #endregion

        #region Login, logout, register

        [HttpPost]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (model.Login != null)
                {
                    model.Login = model.Login.Trim();
                }

                if (_userRegistrationService.ValidateUser(model.Email, model.Password))
                {
                    var user = _userService.GetUserByLogin(model.Login);
                    if (user == null)
                        user = _userService.GetUserByEmail(model.Email);

                    // TODO: migrate shopping cart
                    //_shoppingCartService.MigrateShoppingCart(_workContext.CurrentCustomer, customer);

                    //sign in new user
                    _authService.SignIn(user, true);

                    // when login page redirects to itself
                    if (string.IsNullOrEmpty(returnUrl) || returnUrl.Contains(@"/login?"))
                    {
                        return RedirectToRoute("HomePage");
                    }

                    return RedirectToRoute("HomePage");
                }
                else
                {
                    return RedirectToRoute("Error");
                }
            }

            // we can't be at this point
            return View(model);
        }

        //TODO
        [HttpPost]
        [ChildActionOnly]
        public ActionResult Login(LoginModel model)
        {
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model, string returnUrl)
        {

            if (_workContext.CurrentUser.IsRegistered)
            {
                // Already registered user. 
                _authService.SignOut();
                // Save a new user
                _workContext.CurrentUser = _userService.InsertGuestUser();
            }
            var user = _workContext.CurrentUser;


            if (ModelState.IsValid)
            {
                if (model.Login != null)
                {
                    model.Login = model.Login.Trim();
                }

                var registrationRequest = new UserRegistrationRequest(user, model.Email,
                    model.Login, model.Password, true);
                var registrationResult = _userRegistrationService.RegisterUser(registrationRequest);
                if (registrationResult.Equals("OK"))
                {
                    return RedirectToRoute("HomePage");

                }
                else
                {
                    return RedirectToRoute("Error");
                }
            }

            // smth failed
            return View(model);
        }

        public ActionResult Logout()
        {
            _workContext.CurrentUser = null;

            _authService.SignOut();
            return RedirectToRoute("HomePage");

        }

        #endregion
    }
}