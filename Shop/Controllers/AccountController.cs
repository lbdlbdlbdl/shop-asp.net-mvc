﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Shop.Models;
using System.Data.Entity;
using Shop.WebUI.Models;
using Shop.Domain;
using Shop.Domain.Entities;
using Shop.WebUI.Helpers;
using Shop.WebUI.AuthentificationModule;
using Newtonsoft.Json;
using System.Web.Security;
using Shop.DAL.Repository;

namespace Shop.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private DbContext _context = new ShopContext();

        public AccountController()
        {
        }
        

        [HttpGet]
        [AllowAnonymous]
        public ActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignIn(SignInViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _context.Set<User>().SingleOrDefault(u => u.Login == model.Login);
                if (user == null)
                {
                    model.LoginError = "Login '" + model.Login + "' does not exist";
                }
                else
                {
                    if (Convert.ToBase64String(user.PasswordHash) == Convert.ToBase64String(HashHelper.GetHash(model.Password)))
                    {
                        string role = _context.Set<Role>().SingleOrDefault(r => r.Id == user.RoleId)?.Name;
                        SerializedPrincipal principal = new SerializedPrincipal()
                        {
                            Id = user.Id,
                            Login = user.Login,
                            RoleName = role,
                            FirstName = user.FirstName,
                            SecondName = user.LastName
                        };
                        string serializedPrincipal = JsonConvert.SerializeObject(principal);
                        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                            1,
                            principal.FirstName + " " + principal.SecondName + ", " + principal.Login,
                            DateTime.Now,
                            DateTime.Now.AddMinutes(30),
                            model.RememberMe,
                            serializedPrincipal);
                        string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                        HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                        Response.Cookies.Add(authCookie);
                        return RedirectToAction("Index", "Home");
                    }
                    model.PasswordError = "Invalid password";
                }
            }
            return View(model);
        }

        [Authorize]
        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("SignIn");
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegistrationViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (_context.Set<User>().SingleOrDefault(x => x.Login == model.Login) == null)
                {
                    var users = new UserRepository(_context);
                    users.Add(model.ToUser());
                    return RedirectToAction("Index", "Home");
                }
                model.Error = "User with login '" + model.Login + "' already exist";
            }
            return View(model);
        }
    }
}