﻿using PagedList;
using Shop.Domain;
using Shop.Domain.Entities;
using Shop.WebUI.Helpers;
using Shop.WebUI.Models;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Shop.WebUI.Controllers
{
    public class ItemsController
    {
        private readonly DbContext _context = new ShopContext();

        [AllowAnonymous]
        public ActionResult Index(int page = 1, int pageSize = 10)
        {
            var pagedView = new PagedListViewModel
            {
                ControllerName ="Items",
                PrimaryAttributes = new StaticPagedList<PrimaryAttributesViewModel>(
                    _context.Set<Product>()
                        .Select(x => x)
                        .OrderBy(x => x.Name)
                        .Skip(pageSize * (page - 1))
                        .Take(pageSize)
                        .ToList()
                        .Select(g => g.ToPrimaryAttributesViewModel()),
                    page,
                    pageSize,
                    _context.Set<Product>().Count())

            }

        }
    }
}