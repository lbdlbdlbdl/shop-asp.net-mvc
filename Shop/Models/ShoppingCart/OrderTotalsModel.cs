﻿using Shop.Core.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.WebUI.Models.ShoppingCart
{
    public class OrderTotalsModel : ModelBase
    {
        public bool IsEditable { get; set; }

        public string SubTotal { get; set; }
        
        public bool DisplayWeight { get; set; }
        
        public string Shipping { get; set; }

        public string OrderTotal { get; set; }

        public decimal Weight { get; set; }
    }
}