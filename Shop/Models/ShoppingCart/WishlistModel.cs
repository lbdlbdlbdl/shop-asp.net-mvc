﻿using Shop.Core.Web.Models;
using Shop.WebUI.Models.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.WebUI.Models.ShoppingCart
{
    public class WishlistModel : ModelBase
    {
        public WishlistModel()
        {
            Items = new List<ShoppingCartItemModel>();
        }

        public Guid UserGuid { get; set; }

        public string UserFullname { get; set; }

        public bool ShowProductImages { get; set; }

        public bool IsEditable { get; set; }

        public bool DisplayAddToCart { get; set; }

        public IList<ShoppingCartItemModel> Items { get; set; }
        

        public int ThumbSize { get; set; }
        public int BundleThumbSize { get; set; }
        public bool ShowItemsFromWishlistToCartButton { get; set; }
        

        public partial class ShoppingCartItemModel : EntityModelBase
        {
            public ShoppingCartItemModel()
            {
                Picture = new PictureModel();
            }
            

            public PictureModel Picture { get; set; }

            public int ProductId { get; set; }

            public string ProductName { get; set; }

            public string ProductUrl { get; set; }

            public string UnitPrice { get; set; }

            public string SubTotal { get; set; }

            public int Quantity { get; set; }
            
        }
        
    }
}