﻿using Shop.Core.Web.Models;
using Shop.WebUI.Models.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.WebUI.Models.ShoppingCart
{
    public class MiniShoppingCartModel : ModelBase
    {
        public MiniShoppingCartModel()
        {
            Items = new List<ShoppingCartItemModel>();
        }

        public IList<ShoppingCartItemModel> Items { get; set; }
        public int TotalProducts { get; set; }
        public string SubTotal { get; set; }
        public bool DisplayShoppingCartButton { get; set; }
        public bool CurrentCustomerIsGuest { get; set; }
        public bool AnonymousCheckoutAllowed { get; set; }
        public bool ShowProductImages { get; set; }

        
        public partial class ShoppingCartItemModel : EntityModelBase
        {
            public ShoppingCartItemModel()
            {
                Picture = new PictureModel();
                Items = new List<ShoppingCartItemItem>();
            }

            public int ProductId { get; set; }

            public string ProductName { get; set; }

            public string ShortDesc { get; set; }

            public string ProductUrl { get; set; }

            public int Quantity { get; set; }

            public string UnitPrice { get; set; }

            public PictureModel Picture { get; set; }

            public IList<ShoppingCartItemItem> Items { get; set; }

        }

        public partial class ShoppingCartItemItem : ModelBase
        {
            public string PictureUrl { get; set; }
            public string ProductName { get; set; }
            public string ProductUrl { get; set; }
        }
    }
}