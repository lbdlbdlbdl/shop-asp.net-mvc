﻿using Shop.Core.Web.Models;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Shop.WebUI.Models.User
{
    public class AddressModel : EntityModelBase
    {
        [AllowHtml]
        public string FirstName { get; set; }
        [AllowHtml]
        public string LastName { get; set; }
        [AllowHtml]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public int? CountryId { get; set; }
        [AllowHtml]
        public string CountryName { get; set; }
        [AllowHtml]
        public string City { get; set; }
        [AllowHtml]
        public string Address { get; set; }
        [AllowHtml]
        public string PostalCode { get; set; }
        [AllowHtml]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
    }
}