﻿using Shop.Core.Web.Models;

namespace Shop.WebUI.Models.User
{
    public class RegisterResultModel : ModelBase
    {
        public string Result { get; set; }
    }
}