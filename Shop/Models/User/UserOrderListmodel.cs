﻿using Shop.Core.Web.Models;
using SmartStore.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.WebUI.Models.User
{
    public class UserOrderListmodel : ModelBase
    {

        public PagedList<OrderDetailsModel> Orders { get; set; }
        public UserNavigationModel NavigationModel { get; set; }

        public partial class OrderDetailsModel : EntityModelBase
        {
            public string OrderNumber { get; set; }
            public string OrderTotal { get; set; }
            public string OrderStatus { get; set; }
            public DateTime CreatedOn { get; set; }
        }
        
    }
}