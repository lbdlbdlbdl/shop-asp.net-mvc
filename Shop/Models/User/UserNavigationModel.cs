﻿namespace Shop.WebUI.Models.User
{
    public class UserNavigationModel
    {
        public UserNavigationEnum SelectedTab { get; set; }
    }
    public enum UserNavigationEnum
    {
        Info,
        Address,
        Orders
    }
}