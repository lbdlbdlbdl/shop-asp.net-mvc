﻿using Shop.Core.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Shop.WebUI.Models.User
{
    public class RegisterModel : ModelBase
    {
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        
        [AllowHtml]
        public string Login { get; set; }

        [DataType(DataType.Password)]
        [AllowHtml]
        public string Password { get; set; }

        #region TODO
        // form properties
        //public string Gender { get; set; }
        //public string FirstName { get; set; }
        //public string LastName { get; set; }
        //public string StreetAddress { get; set; }
        #endregion
            
    }
}