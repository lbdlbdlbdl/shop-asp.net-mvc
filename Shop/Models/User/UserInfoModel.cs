﻿using Shop.Core.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Shop.WebUI.Models.User
{
    public class UserInfoModel : ModelBase
    {
        [AllowHtml]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [AllowHtml]
        public string Login { get; set; }
        [AllowHtml]
        public string FirstName { get; set; }
        [AllowHtml]
        public string LastName { get; set; }
        [AllowHtml]
        public string StreetAddress { get; set; }
        [AllowHtml]
        public string PostalCode { get; set; }
        [AllowHtml]
        public string City { get; set; }
        [AllowHtml]
        public string Country { get; set; }
        [AllowHtml]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
        

    }
}