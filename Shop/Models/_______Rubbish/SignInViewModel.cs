﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Shop.WebUI.Models
{
    public class SignInViewModel
    {
        [Required(ErrorMessage = "Enter your Login please")]
        [StringLength(20, ErrorMessage = "Login is too long")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Enter your Password please")]
        [StringLength(20, ErrorMessage = "Password is too long")]
        public string Password { get; set; }

        public string LoginError { get; set; }
        public string PasswordError { get; set; }
        public bool RememberMe { get; set; }
    }
}