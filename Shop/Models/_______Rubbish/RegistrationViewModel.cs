﻿using System.ComponentModel.DataAnnotations;
using Shop.Domain;
using Shop.Domain.Entities;
using Shop.WebUI.Helpers;

namespace Shop.WebUI.Models
{
    public class RegistrationViewModel
    {
        [Required(ErrorMessage = "Enter your email please")]
        [StringLength(40, ErrorMessage = "Email is too long")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Enter your Login please")]
        [StringLength(20, ErrorMessage = "Login is too long")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Enter your Password please")]
        [StringLength(20, ErrorMessage = "Password is too long")]
        [Compare("ConfirmPassword", ErrorMessage = "Passwords do not match")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm your Password please")]
        [StringLength(20, ErrorMessage = "Password confirmation is too long")]
        [Compare("Password", ErrorMessage = "Passwords do not match")]
        public string ConfirmPassword { get; set; }

        public string Error { get; set; }

        public User ToUser()
        {
            return new User()
            {
                Login = this.Login,
                PasswordHash = HashHelper.GetHash(this.Password),
                Email = this.Email,
                RoleId = 2
            };
        }
    }
}