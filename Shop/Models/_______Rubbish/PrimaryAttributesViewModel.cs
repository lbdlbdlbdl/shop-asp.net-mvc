﻿using System.Web;

namespace Shop.WebUI.Models
{
    public class PrimaryAttributesViewModel
    {
        public int Id { get; set; }
        
        public string Name { get; set; }

        public HttpPostedFileBase File { get; set; }

        public byte[] Image { get; set; }
        
        public decimal Price { get; set; }
        
        public string Description { get; set; }

        public int CategoryId { get; set; }
        
        public string Brand { get; set; }

        public bool IsInCart { get; set; }

        public bool IsInWishlist { get; set; }
    }
}