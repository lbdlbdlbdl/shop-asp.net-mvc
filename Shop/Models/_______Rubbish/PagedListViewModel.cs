﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;

namespace Shop.WebUI.Models
{
    public class PagedListViewModel
    {
        public IPagedList<PrimaryAttributesViewModel> PrimaryAttributes { get; set; }
        public string ControllerName { get; set; }
    }
}