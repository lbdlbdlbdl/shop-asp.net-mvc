﻿using Shop.Core.Web.Models;
using Shop.WebUI.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.WebUI.Models.Order
{
    public class OrderDetailsModel : ModelBase
    {
        public OrderDetailsModel()
        {
            Items = new List<OrderItemModel>();
            ShippingAddress = new AddressModel();
        }
        

        public string OrderNumber { get; set; }
        public DateTime CreatedOn { get; set; }
        public string OrderStatus { get; set; }
        public AddressModel ShippingAddress { get; set; }

        public string OrderSubtotal { get; set; }
        public string OrderShipping { get; set; }
        public string OrderTotal { get; set; }
        public string UserComment { get; set; }
        
        
        public IList<OrderItemModel> Items { get; set; }
        
        
        public partial class OrderItemModel : EntityModelBase
        {
            public int ProductId { get; set; }
            public string ProductName { get; set; }
            public string UnitPrice { get; set; }
            public string SubTotal { get; set; }
            public int Quantity { get; set; }
            public string AttributeInfo { get; set; }
        }
        
    }
}