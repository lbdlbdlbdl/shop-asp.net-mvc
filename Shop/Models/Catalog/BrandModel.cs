﻿using Shop.Core.Web.Models;
using Shop.WebUI.Models.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.WebUI.Models.Catalog
{
    public class BrandModel : EntityModelBase
    {
        public BrandModel()
        {
            PictureModel = new PictureModel();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public string MetaTitle { get; set; }

        public PictureModel PictureModel { get; set; }
        public ProductModel Products { get; set; }
    }
}