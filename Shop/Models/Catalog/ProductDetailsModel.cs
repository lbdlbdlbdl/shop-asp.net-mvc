﻿using Shop.Core.Web.Models;
using Shop.WebUI.Models.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Shop.WebUI.Models.Catalog
{
    public class ProductDetailsModel
    {
        private ProductDetailsPictureModel _detailsPictureModel;

        public ProductDetailsModel()
        {
            ProductPrice = new ProductPriceModel();
            AddToCart = new AddToCartModel();
            IsAvailable = true;
        }

        //picture(s)
        public ProductDetailsPictureModel DetailsPictureModel
        {
            get
            {
                if (_detailsPictureModel == null)
                    _detailsPictureModel = new ProductDetailsPictureModel();
                return _detailsPictureModel;
            }
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public string MetaDescription { get; set; }
        public string MetaTitle { get; set; }
        

        public bool ShowBrandName { get; set; }
        public string BrandName { get; set; }
        
        public bool IsAvailable { get; set; }

        public bool IsCurrentUserRegistered { get; set; }

        public ProductPriceModel ProductPrice { get; set; }
        public AddToCartModel AddToCart { get; set; }
        

        public ProductVariantAttributeCombination SelectedCombination { get; set; }
        
        

        public partial class AddToCartModel : ModelBase
        {
            public AddToCartModel()
            {
                this.AllowedQuantities = new List<SelectListItem>();
            }
            public int ProductId { get; set; }
            
            public int EnteredQuantity { get; set; }
            
            public bool DisableBuyButton { get; set; }
            public bool DisableWishlistButton { get; set; }
            public List<SelectListItem> AllowedQuantities { get; set; }
        }

        public partial class ProductPriceModel : ModelBase
        {
            public string Price { get; set; }
            public decimal PriceValue { get; set; }
            public int ProductId { get; set; }
        }

       
    }

    public partial class ProductDetailsPictureModel : ModelBase
    {
        public ProductDetailsPictureModel()
        {
            PictureModels = new List<PictureModel>();
        }

        public string Name { get; set; }
        public PictureModel DefaultPictureModel { get; set; }
        public IList<PictureModel> PictureModels { get; set; }
        public int GalleryStartIndex { get; set; }
    }
}