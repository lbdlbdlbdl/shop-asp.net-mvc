﻿using Shop.Core.Web.Models;
using Shop.WebUI.Models.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.WebUI.Models.Catalog
{
    public class CategoryModel
    {
        //TODO: Add hierarchical category at menu

        public CategoryModel()
        {
            PictureModel = new PictureModel();
            SubCategories = new List<SubCategoryModel>();
        }

        public string Name { get; set; }
        public string FullName { get; set; }
        public string Description { get; set; }
        public string MetaDescription { get; set; }
        public string MetaTitle { get; set; }

        public PictureModel PictureModel { get; set; }

        public IList<SubCategoryModel> SubCategories { get; set; }
        
        public ProductModel Products { get; set; }

        

        public partial class SubCategoryModel : EntityModelBase
        {
            public SubCategoryModel()
            {
                PictureModel = new PictureModel();
            }

            public string Name { get; set; }
            public PictureModel PictureModel { get; set; }
        }
        
    }
}