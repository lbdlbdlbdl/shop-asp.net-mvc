﻿using Shop.Core.PagedLists;
using System.Collections.Generic;

namespace Shop.WebUI.Models.Catalog
{
    public  interface IPagedListActions
    {
        IPageable PagedList { get; }
        IEnumerable<int> AvailablePageSizes { get; }
    }
}