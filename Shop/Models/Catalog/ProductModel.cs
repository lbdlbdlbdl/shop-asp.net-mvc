﻿using System;
using System.Collections.Generic;
using Shop.Core.PagedLists;
using Shop.Core.Web.Models;
using SmartStore.Core;
using Shop.Domain.Entities.Products;
using Shop.WebUI.Models.Media;

namespace Shop.WebUI.Models.Catalog
{
    public class ProductModel : ModelBase, IPagedListActions
    {
        public static ProductModel Empty = new ProductModel(new PagedList<Product>(new List<Product>(), 0, int.MaxValue));

        public ProductModel(IPagedList<Product> products)
        {
            Items = new List<Item>();
            PagedList = products;
        }

        public int? ThumbSize { get; set; }
        public bool ShowDescription { get; set; }
        public bool ShowBrand { get; set; }
        public bool ShowPrice { get; set; }
        public bool ShowButtons { get; set; }
        public bool BuyEnabled { get; set; }

        public IList<Item> Items { get; set; }

        
        public IEnumerable<int> AvailablePageSizes { get; set; }
        public IPageable PagedList { get; set; }
        

        public class Item : EntityModelBase
        {
            public Item(ProductModel parent)
            {
                Parent = parent;
                Price = new PriceModel();
                Picture = new PictureModel();
                Attributes = new List<Attribute>();
            }

            public ProductModel Parent { get; private set; }

            public string Name { get; set; }
            public string Description { get; set; }
            public string StockAvailablity { get; set; }
            

            public BrandModel Brand { get; set; }
            public PriceModel Price { get; set; }
            public PictureModel Picture { get; set; }
            public IList<Attribute> Attributes { get; set; }
            public ColorAttribute ColorAttribute { get; set; }
        }

        public class PriceModel
        {
            public decimal PriceValue { get; set; }
            public string Price { get; set; }
        }

        public class ColorAttribute
        {
            public ColorAttribute(int id, string name, IEnumerable<ColorAttributeValue> values)
            {
                Id = id;
                Name = name;
                Values = new HashSet<ColorAttributeValue>(values);
            }

            public int Id { get; private set; }
            public string Name { get; private set; }
            public ICollection<ColorAttributeValue> Values { get; private set; }
        }

        public class ColorAttributeValue
        {
            public int Id { get; set; }
            public string Color { get; set; }
            public string Alias { get; set; }

            public override int GetHashCode()
            {
                return this.Color.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                var equals = base.Equals(obj);
                if (!equals)
                {
                    var o2 = obj as ColorAttributeValue;
                    if (o2 != null)
                    {
                        equals = string.Compare(this.Color, o2.Color) == 0;
                    }
                }
                return equals;
            }
        }

        public class Attribute
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Alias { get; set; }
        }
    }
}
    

}
}