﻿using Shop.Core.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.WebUI.Models.Media
{
    public class PictureModel : ModelBase
    {
        public int PictureId { get; set; }
        public int? Size { get; set; }
        public string ImageUrl { get; set; }
        public string Title { get; set; }
    }
}