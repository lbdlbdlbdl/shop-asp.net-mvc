﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Core.PagedLists
{
    public interface IPageable : IEnumerable
    {
        int PageIndex { get; set; }
        int PageSize { get; set; }
        int TotalCount { get; set; }
        int PageNumber { get; set; }
        int TotalPages { get; }
        int FirstItemIndex { get; }
        int LastItemIndex { get; }
        bool HasPreviousPage { get; }
        bool HasNextPage { get; }
        bool IsFirstPage { get; }
        bool IsLastPage { get; }
    }
    public interface IPagedList<T> : IPageable, IList<T>
    {
    }
}
