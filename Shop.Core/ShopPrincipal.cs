﻿using Shop.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace Shop.Core
{
    public class ShopPrincipal : IPrincipal
    {
        public ShopPrincipal(User user, string type)
        {
            this.Identity = new ShopIdentity(user.Id, user.Login, type);
        }
        
        public bool IsInRole(string role)
        {
            return (Identity != null && Identity.IsAuthenticated && !string.IsNullOrWhiteSpace(role) && Roles.IsUserInRole(Identity.Name, role));
        }

        public IIdentity Identity { get; private set; }
    }
}
