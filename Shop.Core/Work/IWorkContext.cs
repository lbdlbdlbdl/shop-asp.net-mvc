﻿using Shop.Domain.Entities.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Core.Work
{
    public interface IWorkContext
    {
        User CurrentUser { get; set; }
        bool IsAdmin { get; set; }
    }
}
