﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Core
{
    public class ShopIdentity : ClaimsIdentity
    {
        [SecuritySafeCritical]
        public ShopIdentity(int userId, string name, string type)
            : base(new GenericIdentity(name, type))
        {
            UserId = userId;
        }

        public int UserId { get; private set; }

        public override bool IsAuthenticated
        {
            get
            {
                return UserId != 0;
            }
        }
    }
}
