﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Shop.Core.Web.Models
{
    public sealed class CustomPropertiesDictionary : Dictionary<string, object>
    {
    }
    
    public abstract class ModelBase
    {
        protected ModelBase()
        {
            this.CustomProperties = new CustomPropertiesDictionary();
        }

        public virtual void BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
        }

        /// <summary>
        /// Use this property to store any custom value for your models. 
        /// </summary>
        public CustomPropertiesDictionary CustomProperties { get; set; }
    }

    public class EntityModelBase : ModelBase
    {
        public virtual int Id { get; set; }
    }
}
