﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Core.PagedLists;
using Shop.Domain.Entities;
using Shop.Repository;
using Shop.Domain.Entities.Products;
using SmartStore.Core;
using Shop.Domain.Entities.Media;
using Shop.Services.Pictures.Storage;

namespace Shop.Services.Pictures
{
    public class PictureService : IPictureService
    {
        #region Fields + Ctor

        private readonly IRepository<Picture> _pictureRepository;
        private readonly IRepository<ProductPicture> _productPictureRepository;
        private readonly IMediaStorageProvider _storageProvider;

        private string _staticImagePath;

        public PictureService(
            IRepository<Picture> pictureRepository, 
            IRepository<ProductPicture> productPictureRepository,
            IMediaStorageProvider storageProvider)
        {
            this._pictureRepository = pictureRepository;
            this._productPictureRepository = productPictureRepository;
            this._storageProvider = storageProvider;
        }

        #endregion
        

        public Picture GetPictureById(int pictureId)
        {
            if (pictureId == 0)
                return null;
            var picture = _pictureRepository.GetById(pictureId);
            return picture;
        }

        public IPagedList<Picture> GetPictures(int pageIndex, int pageSize)
        {
            var query = from p in _pictureRepository.GetAll
                        orderby p.Id descending
                        select p;

            var pics = new PagedList<Picture>(query, pageIndex, pageSize);
            return pics;
        }

        public IList<Picture> GetPicturesByProductId(int productId, int recordsToReturn = 0)
        {
            if (productId == 0)
                return new List<Picture>();

            var query = from p in _pictureRepository.GetAll
                        join pp in _productPictureRepository.GetAll on p.Id equals pp.PictureId
                        where pp.ProductId == productId
                        select p;

            if (recordsToReturn > 0)
                query = query.Take(recordsToReturn);

            var pics = query.ToList();
            return pics;
        }

        public Picture InsertPicture(byte[] pictureBinary)
        {
            var picture = _pictureRepository.Create();
            _pictureRepository.Insert(picture);
            // save to storage
            _storageProvider.Save(picture.ToMedia(), pictureBinary);
            return picture;
        }

        public byte[] LoadPictureBinary(Picture picture)
        {
            return _storageProvider.Load(picture.ToMedia());
        }

        public Task<byte[]> LoadPictureBinaryAsync(Picture picture)
        {
            return _storageProvider.LoadAsync(picture.ToMedia());
        }

        public void UpdatePicture(Picture picture, byte[] pictureBinary)
        {
            if (picture == null)
                return;
            _pictureRepository.Update(picture);

            // save to storage
            _storageProvider.Save(picture.ToMedia(), pictureBinary);
        }
    }
}
