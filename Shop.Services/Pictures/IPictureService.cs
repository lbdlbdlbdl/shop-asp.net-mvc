﻿using Shop.Core.PagedLists;
using Shop.Domain.Entities;
using Shop.Domain.Entities.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Services.Pictures
{
    public interface IPictureService
    {
        byte[] LoadPictureBinary(Picture picture);
        Task<byte[]> LoadPictureBinaryAsync(Picture picture);
        IPagedList<Picture> GetPictures(int pageIndex, int pageSize);
        Picture GetPictureById(int pictureId);
        IList<Picture> GetPicturesByProductId(int productId, int recordsToReturn = 0);
        Picture InsertPicture(byte[] pictureBinary);
    }
}
