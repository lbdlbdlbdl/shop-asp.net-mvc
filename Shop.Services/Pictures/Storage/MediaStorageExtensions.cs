﻿using Shop.Domain;
using Shop.Domain.Entities.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Services.Pictures.Storage
{
    public static class MediaStorageExtensions
    {
        /// <summary>
        /// Converts a picture entity into a media storage item
        /// </summary>
        public static MediaItem ToMedia(this Picture picture)
        {
            var media = new MediaItem
            {
                Entity = picture,
                Path = ""
            };

            return media;
        }

        /// <summary>
		/// Create a file name for a media item
		/// </summary>
		public static string GetFileName(this MediaItem media)
        {
            if (media != null)
            {
                var extension = media.FileExtension;
                
                var baseEntity = media.Entity as BaseEntity;

                var fileName = string.Format("{0}-0{1}",
                    baseEntity.Id.ToString("0000000"),
                    extension.EmptyNull().MustStartsWith(".")
                );

                return fileName;
            }
            return null;
        }

        public static string EmptyNull(this string value)
        {
            return (value ?? string.Empty).Trim();
        }

        public static string MustStartsWith(this string value, string startsWith)
        {
            return value.StartsWith(startsWith) ? value : (startsWith + value);
        }
    }
}
