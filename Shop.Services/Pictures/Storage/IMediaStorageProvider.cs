﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Services.Pictures.Storage
{
    public interface IMediaStorageProvider
    {
        /// <summary>
     /// Loads media item data
     /// </summary>
     /// <param name="media">Media storage item</param>
        byte[] Load(MediaItem media);

        /// <summary>
        /// Asynchronously loads media item data
        /// </summary>
        /// <param name="media">Media storage item</param>
        Task<byte[]> LoadAsync(MediaItem media);

        /// <summary>
        /// Saves media item data
        /// </summary>
        /// <param name="media">Media storage item</param>
        /// <param name="data">New binary data</param>
        void Save(MediaItem media, byte[] data);

        /// <summary>
        /// Asynchronously saves media item data
        /// </summary>
        /// <param name="media">Media storage item</param>
        /// <param name="data">New binary data</param>
        Task SaveAsync(MediaItem media, byte[] data);
        
    }
}
