﻿using Shop.Domain.Entities.Media;
using Shop.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Services.Pictures.Storage
{
    public class DatabaseMediaStorageProvider : IMediaStorageProvider
    {
        #region Fields+Ctor

        private readonly IRepository<MediaStorage> _mediaStorageRepository;
        private readonly DbContext _dbContext;

        public DatabaseMediaStorageProvider(
            IRepository<MediaStorage> mediaStorageRepository,
            DbContext dbContext)
        {
            _mediaStorageRepository = mediaStorageRepository;
            _dbContext = dbContext;
        }

        #endregion

        public byte[] Load(MediaItem media)
        {
            if ((media.Entity.MediaStorageId ?? 0) != 0 && media.Entity.MediaStorage != null)
            {
                return media.Entity.MediaStorage.Data;
            }

            return new byte[0];
        }

        public Task<byte[]> LoadAsync(MediaItem media)
        {
            return Task.FromResult(Load(media));
        }
        

        public void Save(MediaItem media, byte[] data)
        {
            if (data == null || data.LongLength == 0)
            {
                // remove media storage if any
                if ((media.Entity.MediaStorageId ?? 0) != 0 && media.Entity != null && media.Entity.MediaStorage != null)
                {
                    _mediaStorageRepository.Delete(media.Entity.MediaStorage);
                }
            }
            else
            {
                if (media.Entity.MediaStorage == null)
                {
                    // entity has no media storage -> insert
                    var newStorage = new MediaStorage { Data = data };

                    _mediaStorageRepository.Insert(newStorage);

                    media.Entity.MediaStorageId = newStorage.Id;
                    _dbContext.SaveChanges();
                }
                else
                {
                    // update existing media storage
                    media.Entity.MediaStorage.Data = data;
                    _mediaStorageRepository.Update(media.Entity.MediaStorage);
                }
            }
        }

        public Task SaveAsync(MediaItem media, byte[] data)
        {
            Save(media, data);
            return Task.FromResult(0);
        }
    }
}
