﻿using Shop.Domain.Entities.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Services.Pictures.Storage
{
    public class MediaItem
    {
        /// <summary>
        /// Entity of the media storage item
        /// </summary>
        public IMedia Entity { get; set; }

        /// <summary>
        /// Storage path
        /// </summary>
        public string Path { get; set; }
        
        /// <summary>
        /// File extension
        /// </summary>
        public string FileExtension { get; set; }
    }
}
