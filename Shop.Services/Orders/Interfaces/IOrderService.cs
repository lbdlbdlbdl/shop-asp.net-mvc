﻿using Shop.Core;
using Shop.Core.PagedLists;
using Shop.Domain.Entities.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Services.Orders.Interfaces
{
    public interface IOrderService
    {
        #region Orders

        Order GetOrderById(int orderId);
        Order GetOrderByNumber(string orderNumber);
        IList<Order> GetOrdersByIds(int[] orderIds);
        void DeleteOrder(Order order);
        IPagedList<Order> GetAllOrders(int pageIndex, int pageSize);
        void InsertOrder(Order order);
        void UpdateOrder(Order order);

        #endregion

        #region OrderItems

        OrderItem GetOrderItemById(int orderItemId);
        IList<OrderItem> GetAllOrderItems(int? orderId,
           int? customerId, DateTime? startTime, DateTime? endTime,
           OrderStatus? os, bool loadDownloableProductsOnly = false);
        void DeleteOrderItem(OrderItem orderItem);

        #endregion
    }
}
