﻿using Shop.Services.Orders.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Core;
using Shop.Domain.Entities.Orders;
using Shop.Repository;
using Shop.Domain.Entities.Products;
using Shop.Domain.Entities.Users;
using Shop.Core.PagedLists;

namespace Shop.Services.Orders
{
    public class OrderService : IOrderService
    {
        #region Fields + Ctor

        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<OrderItem> _orderItemRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<User> _userRepository;

        public OrderService(IRepository<Order> orderRepository,
            IRepository<OrderItem> orderItemRepository,
            IRepository<Product> productRepository,
            IRepository<User> userRepository)
        {
            this._orderRepository = orderRepository;
            this._orderItemRepository = orderItemRepository;
            this._productRepository = productRepository;
            this._userRepository = userRepository;
        }

        #endregion

        #region Methods

        #region Orders

        public Order GetOrderById(int orderId)
        {
            if (orderId == 0)
                return null;

            return _orderRepository.GetById(orderId);
        }

        public void DeleteOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");
            order.Deleted = true;
            UpdateOrder(order);
        }

        public IPagedList<Order> GetAllOrders(int pageIndex, int pageSize)
        {
            var query = _orderRepository.GetAll;
            query = query.OrderByDescending(o => o.CreatedOnUtc);

            var orders = new PagedList<Order>(query, pageIndex, pageSize);
            return orders;
        }

        public Order GetOrderByNumber(string orderNumber)
        {
            if (string.IsNullOrWhiteSpace(orderNumber))
            {
                return null;
            }

            var query = from o in _orderRepository.GetAll
                        where o.OrderNumber == orderNumber
                        select o;
            var order = query.FirstOrDefault();

            int id = 0;
            if (order == null && int.TryParse(orderNumber, out id) && id > 0)
            {
                return this.GetOrderById(id);
            }

            return order;
        }

        public IList<Order> GetOrdersByIds(int[] orderIds)
        {
            if (orderIds == null || orderIds.Length == 0)
                return new List<Order>();

            var query = from o in _orderRepository.GetAll
                        where orderIds.Contains(o.Id)
                        select o;
            var orders = query.ToList();
            //sort by passed identifiers
            var sortedOrders = new List<Order>();
            foreach (int id in orderIds)
            {
                var order = orders.Find(x => x.Id == id);
                if (order != null)
                    sortedOrders.Add(order);
            }
            return sortedOrders;
        }

        public void InsertOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");
            _orderRepository.Insert(order);
        }

        public virtual void UpdateOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");
            _orderRepository.Update(order);
        }

        #endregion

        #region Order Items 

        public void DeleteOrderItem(OrderItem orderItem)
        {
            if (orderItem == null)
                throw new ArgumentNullException("orderItem");
            int orderId = orderItem.OrderId;
            _orderItemRepository.Delete(orderItem);
        }

        public IList<OrderItem> GetAllOrderItems(
            int? orderId,
            int? userId,
            DateTime? startTime,
            DateTime? endTime,
            OrderStatus? os)
        {
            int? orderStatusId = null;
            if (os.HasValue)
                orderStatusId = (int)os.Value;

            var query = from orderItem in _orderItemRepository.GetAll
                        join o in _orderRepository.GetAll on orderItem.OrderId equals o.Id
                        join p in _productRepository.GetAll on orderItem.ProductId equals p.Id
                        where (!orderId.HasValue || orderId.Value == 0 || orderId == o.Id) &&
                        (!userId.HasValue || userId.Value == 0 || userId == o.UserId) &&
                        (!startTime.HasValue || startTime.Value <= o.CreatedOnUtc) &&
                        (!endTime.HasValue || endTime.Value >= o.CreatedOnUtc) &&
                        !o.Deleted
                        orderby o.CreatedOnUtc descending, orderItem.Id
                        select orderItem;
            var orderItems = query.ToList();
            return orderItems;
        }


        public OrderItem GetOrderItemById(int orderItemId)
        {
            if (orderItemId == 0)
                return null;
            return _orderItemRepository.GetById(orderItemId);
        }

        #endregion


        public decimal GetOrderSubTotal(Order order)
        {
            var subTotal = decimal.Zero;
            if (order.OrderItems.Count == 0)
                return 0;
            foreach (var orderItem in order.OrderItems)
            {
                if (orderItem.Product == null)
                    continue;
                decimal itemPrice = orderItem.Product.Price;
                subTotal = subTotal + decimal.Multiply(itemPrice, orderItem.Quantity);
            }
            return (subTotal < decimal.Zero ? decimal.Zero : subTotal);
        }

        #endregion
    }
}
