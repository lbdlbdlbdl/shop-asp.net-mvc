﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Core.Work;

namespace Shop.Services
{
    public class CommonServices : ICommonServices
    {
        private readonly Lazy<IWorkContext> _workContext;

        public IWorkContext WorkContext
        {
            get
            {
                return _workContext.Value;
            }
        }
    }
}
