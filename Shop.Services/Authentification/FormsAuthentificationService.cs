﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Domain.Entity;
using Shop.Services.Users;
using System.Web;
using System.Web.Security;
using Shop.Core;

namespace Shop.Services.Authentification
{
    public class FormsAuthentificationService : IAuthentificationService
    {
        private readonly HttpContextBase _httpContext;
        private readonly IUserService _userService;
        private readonly TimeSpan _expirationTimeSpan;
        private User _cachedUser;

        public FormsAuthentificationService(HttpContextBase httpContext, IUserService userService)
        {
            this._httpContext = httpContext;
            this._userService = userService;
            this._expirationTimeSpan = FormsAuthentication.Timeout;
        }

        public void SignIn(User user, bool createPersistentCookie)
        {
            var now = DateTime.UtcNow.ToLocalTime();
            var ticket = new FormsAuthenticationTicket(
                1,
                user.Login,
                now,
                now.Add(_expirationTimeSpan),
                createPersistentCookie,
                user.Login,
                FormsAuthentication.FormsCookiePath);

            var encryptedTicket = FormsAuthentication.Encrypt(ticket);

            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            cookie.HttpOnly = true;
            if (ticket.IsPersistent)
            {
                cookie.Expires = ticket.Expiration;
            }
            cookie.Secure = FormsAuthentication.RequireSSL;
            cookie.Path = FormsAuthentication.FormsCookiePath;
            if (FormsAuthentication.CookieDomain != null)
            {
                cookie.Domain = FormsAuthentication.CookieDomain;
            }

            _httpContext.Response.Cookies.Add(cookie);
            _cachedUser = user;
        }

        public void SignOut()
        {
            _cachedUser = null;
            FormsAuthentication.SignOut();
        }

        public virtual User GetAuthenticatedUser()
        {
            if (_cachedUser != null)
                return _cachedUser;

            if (_httpContext == null || _httpContext.Request == null || !_httpContext.Request.IsAuthenticated || _httpContext.User == null)
                return null;

            User user = null;
            FormsIdentity formsIdentity = null;
            ShopIdentity shopIdentity = null;

            if ((formsIdentity = _httpContext.User.Identity as FormsIdentity) != null)
            {
                user = GetAuthenticatedUserFromTicket(formsIdentity.Ticket);
            }
            else if ((shopIdentity = _httpContext.User.Identity as ShopIdentity) != null)
            {
                user = _userService.GetUserById(shopIdentity.UserId);
            }

            if (user != null && user.IsRegistered())
            {
                _cachedUser = user;
            }

            return _cachedUser;
        }
        public virtual User GetAuthenticatedUserFromTicket(FormsAuthenticationTicket ticket)
        {
            if (ticket == null)
                throw new ArgumentNullException("ticket");

            var login = ticket.UserData;

            if (String.IsNullOrWhiteSpace(login))
                return null;

            var user = _userService.GetUserByLogin(login);
            return user;
        }
    }

}
