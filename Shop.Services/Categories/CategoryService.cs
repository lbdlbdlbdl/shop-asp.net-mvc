﻿using Shop.Core.Work;
using Shop.Domain.Entities.Products;
using Shop.Repository;
using Shop.Services.Products;
using Shop.Services.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Core.PagedLists;
using SmartStore.Core;

namespace Shop.Services.Categories
{
    public class CategoryService : ICategoryService
    {
        #region Field + Ctor

        private readonly IRepository<Category> _categoryRepository;
        private readonly IRepository<ProductCategory> _productCategoryRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IWorkContext _workContext;
        private readonly IUserService _userService;
        private readonly IProductService _productService;

        public CategoryService(
            IRepository<Category> categoryRepository,
            IRepository<ProductCategory> productCategoryRepository,
            IRepository<Product> productRepository,
            IWorkContext workContext,
            IUserService userService,
            IProductService productService)
        {
            this._categoryRepository = categoryRepository;
            this._productRepository = productRepository;
            this._productService = productService;
            this._userService = userService;
            this._workContext = workContext;
            this._productCategoryRepository = productCategoryRepository;
        }

        #endregion

        #region Methods

        #region Category

        public void DeleteCategory(Category category, bool deleteChilds = false)
        {
            if (category == null)
                throw new ArgumentNullException("category");

            category.Deleted = true;
            UpdateCategory(category);

            var childCategories = GetAllCategoriesByParentCategoryId(category.Id);
            DeleteAllCategories(childCategories, deleteChilds);
        }

        public void DeleteAllCategories(IList<Category> categories, bool delete)
        {
            foreach (var category in categories)
            {
                if (delete)
                    category.Deleted = true;
                else
                    category.ParentCategoryId = 0;

                UpdateCategory(category);

                var childCategories = GetAllCategoriesByParentCategoryId(category.Id);
                DeleteAllCategories(childCategories, delete);
            }
        }

        public IQueryable<Category> GetCategories(string categoryName = "")
        {
            var query = _categoryRepository.GetAll;
            if (!string.IsNullOrEmpty(categoryName))
                query = query.Where(c => c.Name.Contains(categoryName));
            query = query.Where(c => !c.Deleted);
            return query;
        }

        public IPagedList<Category> GetAllCategories(
            string categoryName = "",
            int pageIndex = 0,
            int pageSize = int.MaxValue,
            bool applyNavigationFilters = true,
            bool ignoreCategoriesWithoutExistingParent = true)
        {
            var query = GetCategories(categoryName);
            query = query
                .OrderBy(x => x.ParentCategoryId)
                .ThenBy(x => x.Name);
            var unsortedCategories = query.ToList();

            // sort categories
            var sortedCategories = unsortedCategories.SortCategoriesForTree(ignoreCategoriesWithoutExistingParent: ignoreCategoriesWithoutExistingParent);

            // paging
            return new PagedList<Category>(sortedCategories, pageIndex, pageSize);
        }

        public IList<Category> GetAllCategoriesByParentCategoryId(int parentCategoryId)
        {
            var query = _categoryRepository.GetAll;
            query = query.Where(c => c.ParentCategoryId == parentCategoryId);
            query = query.Where(c => !c.Deleted);

            var categories = query.ToList();
            return categories;
        }

        public Category GetCategoryById(int categoryId)
        {
            if (categoryId == 0)
                return null;
            return _categoryRepository.GetById(categoryId);
        }

        public void InsertCategory(Category category)
        {
            if (category == null)
                throw new ArgumentNullException("category");
            _categoryRepository.Insert(category);
        }

        public void UpdateCategory(Category category)
        {
            if (category == null)
                throw new ArgumentNullException("category");

            // validate category
            var parentCategory = GetCategoryById(category.ParentCategoryId);
            while (parentCategory != null)
            {
                if (category.Id == parentCategory.Id)
                {
                    category.ParentCategoryId = 0;
                    break;
                }
                parentCategory = GetCategoryById(parentCategory.ParentCategoryId);
            }

            _categoryRepository.Update(category);
        }


        #endregion

        #region Product Category

        public void DeleteProductCategory(ProductCategory productCategory)
        {
            if (productCategory == null)
                throw new ArgumentNullException("productCategory");

            _productCategoryRepository.Delete(productCategory);
        }

        public IPagedList<ProductCategory> GetProductCategoriesByCategoryId(int categoryId, int pageIndex, int pageSize)
        {
            if (categoryId == 0)
                return new PagedList<ProductCategory>(new List<ProductCategory>(), pageIndex, pageSize);
            var query = from pc in _productCategoryRepository.GetAll
                        join p in _productRepository.GetAll on pc.ProductId equals p.Id
                        where pc.CategoryId == categoryId && !p.Deleted
                        select pc;

            query = query
                .OrderBy(pc => pc.Category)
                .ThenBy(pc => pc.Id);   // required for paging

            var productCategories = new PagedList<ProductCategory>(query, pageIndex, pageSize);

            return productCategories;
        }

        public IList<ProductCategory> GetProductCategoriesByProductId(int productId)
        {
            if (productId == 0)
                return new List<ProductCategory>();

            var query = from pc in _productCategoryRepository.GetAll.Expand(x => x.Category) //query.Include(path);
                        join c in _categoryRepository.GetAll on pc.CategoryId equals c.Id
                        where pc.ProductId == productId &&
                              !c.Deleted
                        orderby pc.Id
                        select pc;

            var allProductCategories = query.ToList();
            var result = new List<ProductCategory>();
            result.AddRange(allProductCategories);
            return result;
        }

        public ProductCategory GetProductCategoryById(int productCategoryId)
        {
            if (productCategoryId == 0)
                return null;
            return _productCategoryRepository.GetById(productCategoryId);
        }

        public void InsertProductCategory(ProductCategory productCategory)
        {
            if (productCategory == null)
                throw new ArgumentNullException("productCategory");
            _productCategoryRepository.Insert(productCategory);
        }

        public void UpdateProductCategory(ProductCategory productCategory)
        {
            if (productCategory == null)
                throw new ArgumentNullException("productCategory");
            _productCategoryRepository.Update(productCategory);
        }

        #endregion

        #endregion
    }
}
