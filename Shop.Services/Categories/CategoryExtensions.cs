﻿using Shop.Domain;
using Shop.Domain.Entities.Products;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Services.Categories
{
    public static class CategoryExtensions
    {
        /// <summary>
        /// Sort categories for tree representation
        /// </summary>
        public static IList<Category> SortCategoriesForTree(
            this IList<Category> source, 
            int parentId = 0, 
            bool ignoreCategoriesWithoutExistingParent = false)
        {
            if (source == null)
                throw new ArgumentNullException("source");
            var result = new List<Category>();
            var categories = source.ToList().FindAll(c => c.ParentCategoryId == parentId);
            foreach (var category in categories)
            {
                result.Add(category);
                result.AddRange(SortCategoriesForTree(source, category.Id, true));
            }
            if (!ignoreCategoriesWithoutExistingParent && result.Count != source.Count)
            {
                // find categories without parent in provided category source and insert them into result
                foreach (var cat in source)
                    if (result.Where(x => x.Id == cat.Id).FirstOrDefault() == null)
                        result.Add(cat);
            }
            return result;
        }

        /// <summary>
		/// Instructs the repository to eager load entities that may be in the type's association path.
		/// </summary>
		/// <param name="query">A previously created query object which the expansion should be applied to.</param>
		/// <param name="path">The path of the child entities to eager load.</param>
		/// <returns>A new query object to which the expansion was applied.</returns>
        public static IQueryable<T> Expand<T, TProperty>(this IQueryable<T> query, Expression<Func<T, TProperty>> path) where T : BaseEntity
        {
            return query.Include(path);
        }

    }
}
