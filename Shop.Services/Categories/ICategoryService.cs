﻿using Shop.Core.PagedLists;
using Shop.Domain.Entities.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Services.Categories
{
    public interface ICategoryService
    {
        #region Category

        void DeleteCategory(Category category, bool deleteChilds = false);
        void DeleteAllCategories(IList<Category> categories, bool delete);
        IQueryable<Category> GetCategories(string categoryName = "");
        IList<Category> GetAllCategoriesByParentCategoryId(int parentCategoryId);
        Category GetCategoryById(int categoryId);
        void InsertCategory(Category category);
        void UpdateCategory(Category category);

        #endregion

        #region Product Category

        void DeleteProductCategory(ProductCategory productCategory);
        IPagedList<ProductCategory> GetProductCategoriesByCategoryId(int categoryId, int pageIndex, int pageSize);
        IList<ProductCategory> GetProductCategoriesByProductId(int productId);
        ProductCategory GetProductCategoryById(int productCategoryId);
        void InsertProductCategory(ProductCategory productCategory);
        void UpdateProductCategory(ProductCategory productCategory);

        #endregion
    }
}
