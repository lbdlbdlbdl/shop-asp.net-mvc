﻿using Shop.Domain;
using Shop.Domain.Entities.Users;
using Shop.Services.Security;
using Shop.Services.Users.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Services.Users
{
    public class UserRegistrationService : IUserRegistrationService
    {
        private readonly IUserService _userService;
        private readonly IEncryptionService _encryptionService;

        public UserRegistrationService(IUserService userService, IEncryptionService encryptionService)
        {
            this._userService = userService;
            this._encryptionService = encryptionService;
        }

        public string RegisterUser(UserRegistrationRequest request)
        {
            string error;
            if (request.User.IsRegistered())
            {
                error = "AlreadyRegistered";
                return error;
            }
            if (String.IsNullOrEmpty(request.Email))
            {
                error = "EmailIsNullOrEmpty";
                return error;
            }
            if (!request.Email.IsEmail())
            {
                error = "WrongEmail";
                return error;
            }
            // us unique
            if (_userService.GetUserByEmail(request.Email) != null)
            {
                error = "EmailAlreadyExists";
                return error;
            }
            if (_userService.GetUserByLogin(request.Login) != null)
            {
                error = "LoginAlreadyExists";
                return error;
            }

            // request is valid
            request.User.Login = request.Login;
            request.User.Email = request.Email;
            request.User.PasswordState = request.PasswordState;

            switch (request.PasswordState)
            {
                case PasswordState.Clear:
                    request.User.Password = request.Password;
                    break;
                case PasswordState.Hashed:
                    string saltKey = _encryptionService.CreateSaltKey(5);
                    request.User.PasswordSalt = saltKey;
                    request.User.Password = _encryptionService.CreatePasswordHash(request.Password, saltKey, "SHA1");
                    break;
            }
            request.User.Active = request.IsApproved;
            request.User.IsRegistered = true;
            request.User.UserRoles.Add(new UserRole
            {
                SystemName = SystemUserRoleNames.Registered
            });

            //remove from 'Guests' role
            var guestRole = request.User.UserRoles.FirstOrDefault(ur => ur.SystemName == SystemUserRoleNames.Guest);
            if (guestRole != null)
            {
                request.User.UserRoles.Remove(guestRole);
            }
            _userService.UpdateUser(request.User);

            return "OK";
        }

        public bool ValidateUser(string loginOrEmail, string password)
        {
            User user = null;
            user = _userService.GetUserByLogin(loginOrEmail);
            if (user == null)
                return false;

            //only registered can login
            if (!user.IsRegistered())
                return false;

            string pwd = "";
            switch (user.PasswordState)
            {
                case PasswordState.Hashed:
                    pwd = _encryptionService.CreatePasswordHash(password, user.PasswordSalt, "SHA1");
                    break;
                default:
                    pwd = password;
                    break;
            }

            bool isValid = pwd == user.Password;

            return isValid;
        }
    }
}
