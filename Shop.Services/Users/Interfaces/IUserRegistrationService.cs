﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Services.Users.Interfaces
{
    public interface IUserRegistrationService
    {
        bool ValidateUser(string loginOrEmail, string password);
        string RegisterUser(UserRegistrationRequest request);
    }
}
