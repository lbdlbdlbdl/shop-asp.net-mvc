﻿using Shop.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Domain.Entities.Users;

namespace Shop.Services.Users
{
    public interface IUserService
    {
        void DeleteUser(User user);
        User GetUserById(int userId);
        IList<User> GetUsersByIds(int[] userIds);
        User GetUserByEmail(string email);
        User GetUserByLogin(string login);
        void InsertUser(User user);
        User InsertGuestUser();
        void UpdateUser(User user);
    }
}
