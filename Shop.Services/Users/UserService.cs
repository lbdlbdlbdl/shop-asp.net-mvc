﻿using Shop.Domain;
using Shop.Domain.Entities.Users;
using Shop.Domain.Entity;
using Shop.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Services.Users
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<UserRole> _userRoleRepository;

        public UserService(IRepository<User> userRepository)
        {
            this._userRepository = userRepository;
        }


        #region User

        #region Get

        public User GetUserByEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return null;

            var query = from c in IncludeShoppingCart(_userRepository.GetAll)
                        orderby c.Id
                        where c.Email == email
                        select c;
            var user = query.FirstOrDefault();
            return user;
        }

        public User GetUserById(int userId)
        {
            if (userId == 0)
                return null;
            // var user = _userRepository.GetById(userId);
            var user = IncludeShoppingCart(_userRepository.GetAll).SingleOrDefault(x => x.Id == userId);
            return user;
        }

        public User GetUserByLogin(string login)
        {
            if (string.IsNullOrWhiteSpace(login))
                return null;

            var query = from c in IncludeShoppingCart(_userRepository.GetAll)
                        orderby c.Id
                        where c.Login == login
                        select c;

            var user = query.FirstOrDefault();
            return user;
        }

        public IList<User> GetUsersByIds(int[] userIds)
        {
            if (userIds == null || userIds.Length == 0)
                return new List<User>();

            var query = from c in _userRepository.GetAll
                        where userIds.Contains(c.Id)
                        select c;

            var users = query.ToList();

            //sort
            var sortedUsers = new List<User>();
            foreach (int id in userIds)
            {
                var user = users.Find(x => x.Id == id);
                if (user != null)
                    sortedUsers.Add(user);
            }
            return sortedUsers;
        }

        #endregion

        public void InsertUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            _userRepository.Insert(user);
        }

        public virtual User InsertGuestUser()
        {
            var user = new User
            {
                Active = true,
                LastActivityDateUtc = DateTime.UtcNow,
            };
            
            var guestRole = GetUserRoleBySystemName(SystemUserRoleNames.Guest);
            user.UserRoles.Add(guestRole);
            _userRepository.Insert(user);

            return user;
        }


        public void UpdateUser(User user)
        {
            _userRepository.Update(user);
        }

        public void DeleteUser(User user)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region User Role

        public virtual UserRole GetUserRoleById(int userRoleId)
        {
            if (userRoleId == 0)
                return null;
            return _userRoleRepository.GetById(userRoleId);
        }

        public virtual UserRole GetUserRoleBySystemName(string systemName)
        {
            if (String.IsNullOrWhiteSpace(systemName))
                return null;

            var query = from cr in _userRoleRepository.GetAll
                        orderby cr.Id
                        where cr.SystemName == systemName
                        select cr;

            var customerRole = query.FirstOrDefault();
            return customerRole;
        }

        #endregion

        private IQueryable<User> IncludeShoppingCart(IQueryable<User> query)
        {
            return query.Include(x => x.ShoppingCartItems.Select(y => y.Product));
        }

    }
}
