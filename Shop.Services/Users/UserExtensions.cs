﻿using Shop.Domain;
using Shop.Domain.Entities.ShoppingCarts;
using Shop.Domain.Entities.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Services.Users
{
    public static class UserExtensions
    {
        #region User

        public static bool IsInUserRole(this User user, string userRoleSystemName, bool onlyActiveCustomerRoles = true)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            if (String.IsNullOrEmpty(userRoleSystemName))
                throw new ArgumentNullException("userRoleSystemName");

            var result = user.UserRoles.Where(ur => !onlyActiveCustomerRoles || ur.Active)
                .Where(ur => ur.SystemName == userRoleSystemName)
                .FirstOrDefault() != null;
            return result;
        }

        public static bool IsRegistered(this User user, bool onlyActiveUserRoles = true)
        {
            return IsInUserRole(user, SystemUserRoleNames.Registered, onlyActiveUserRoles);
        }

        public static bool IsAdmin(this User user, bool onlyActiveUserRoles = true)
        {
            return IsInUserRole(user, SystemUserRoleNames.Administrator, onlyActiveUserRoles);
        }

        public static bool IsGuest(this User user, bool onlyActiveUserRoles = true)
        {
            return IsInUserRole(user, SystemUserRoleNames.Guest, onlyActiveUserRoles);
        }

        public static string GetFullName(this User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            var firstName = user.UserInfo.FirstName;
            var lastName = user.UserInfo.LastName;

            string fullName = "";
            if (!String.IsNullOrWhiteSpace(firstName) && !String.IsNullOrWhiteSpace(lastName))
            {
                fullName = string.Format("{0} {1}", firstName, lastName);
            }
            else
            {
                if (!String.IsNullOrWhiteSpace(firstName))
                    fullName = firstName;

                if (!String.IsNullOrWhiteSpace(lastName))
                    fullName = lastName;

                if (String.IsNullOrWhiteSpace(firstName) && String.IsNullOrWhiteSpace(lastName))
                {
                    var address = user.Address;
                    if (address != null)
                        fullName = string.Format("{0} {1}", address.FirstName, address.LastName);
                }
            }
            return fullName;
        }

        #endregion

        #region ShoppingCart

        public static int CountProductsInCart(this User customer, ShoppingCartType cartType)
        {
            int count = customer.ShoppingCartItems.Sum(x => x.Quantity);
            return count;
        }
        public static List<ShoppingCartItem> GetCartItems(this User user, ShoppingCartType cartType)
        {
            var items = user.ShoppingCartItems.AsQueryable();

            var organizedItems = items
                .OrderByDescending(x => x.Id)
                .ToList();

            return organizedItems.ToList();
        }
        
        #endregion
    }
}
