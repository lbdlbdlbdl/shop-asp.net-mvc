﻿using Shop.Domain.Entities.Users;

namespace Shop.Services.Users
{
    public class UserRegistrationRequest
    {
        public User User { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public PasswordState PasswordState { get; set; }
        public bool IsApproved { get; set; }

        public UserRegistrationRequest(User user, string email, string login,
            string password,
            bool isApproved = true)
        {
            this.User = user;
            this.Email = email;
            this.Login = login;
            this.Password = password;
            this.IsApproved = isApproved;
        }
    }
}
