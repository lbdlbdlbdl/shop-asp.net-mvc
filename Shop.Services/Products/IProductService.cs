﻿using Shop.Core.PagedLists;
using Shop.Domain.Entities.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Services.Products
{
    public interface IProductService
    {
        void DeleteProduct(Product product);
        Product GetProductById(int productId);
        void InsertProduct(Product product);
        void UpdateProduct(Product product);
        int CountProducts(ProductSearchContext productSearchContext);
        IPagedList<Product> SearchProducts(ProductSearchContext productSearchContext);
        Product GetProductByBrandName(string brandName);
        Product GetProductByName(string name);
    }
}
