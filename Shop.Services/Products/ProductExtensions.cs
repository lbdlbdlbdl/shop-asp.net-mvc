﻿using Shop.Domain.Entities;
using Shop.Domain.Entities.Media;
using Shop.Domain.Entities.Products;
using Shop.Services.Pictures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Services.Products
{
    public static class ProductExtensions
    {
        public static Picture GetDefaultProductPicture(this Product source, IPictureService pictureService)
        {
            var picture = pictureService.GetPicturesByProductId(source.Id, 1).FirstOrDefault();
            return picture;
        }
    }
}
