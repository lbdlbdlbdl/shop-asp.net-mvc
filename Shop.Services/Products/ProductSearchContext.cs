﻿using Shop.Domain.Entities.Products;
using System.Collections.Generic;
using System.Linq;

namespace Shop.Services.Products
{
    public class ProductSearchContext
    {
        public ProductSearchContext()
        {
            CategoryIds = new List<int>();
            PageSize = 12;
        }
        
        public IQueryable<Product> Query { get; set; }
        public IList<int> CategoryIds { get; set; }
        public IList<int> ProductIds { get; set; }
        
        public int BrandId { get; set; }
        public bool MatchAllcategories { get; set; }

        /// <summary>
        /// Product tag identifier; 0 to load all records
        /// </summary>
        public int ProductTagId { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        
        /// <summary>
        /// Parent product identifier (used with grouped products); 0 to load all records
        /// </summary>
        public int ParentGroupedProductId { get; set; }
        
    }
}