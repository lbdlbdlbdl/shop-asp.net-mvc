﻿using Shop.Core.PagedLists;
using Shop.Domain.Entities.Products;
using Shop.Domain.Entities.ShoppingCarts;
using Shop.Repository;
using SmartStore.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Services.Products
{
    public class ProductService : IProductService
    {

        #region Field + Ctor

        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<ProductPicture> _productPictureRepository;
        private readonly IRepository<ShoppingCartItem> _sciRepository;
        private readonly ICommonServices _services;

        public ProductService(
            IRepository<Product> productRepository,
            IRepository<ProductPicture> productPictureRepository,
            IRepository<ShoppingCartItem> sciRepository)
        {
            this._productRepository = productRepository;
            this._productPictureRepository = productPictureRepository;
            this._sciRepository = sciRepository;
        }

        #endregion

        #region Methods

        #region Product 
        public void DeleteProduct(Product product)
        {
            if (product == null)
                throw new ArgumentNullException("product");
            product.Deleted = true;
            UpdateProduct(product);
        }

        public Product GetProductById(int productId)
        {
            if (productId == 0)
                return null;
            return _productRepository.GetById(productId);
        }

        public void InsertProduct(Product product)
        {

            if (product == null)
                throw new ArgumentNullException("product");
            //insert
            _productRepository.Insert(product);

        }

        public void UpdateProduct(Product product)
        {
            if (product == null)
                throw new ArgumentNullException("product");
            // update
            _productRepository.Update(product);
        }

        public int CountProducts(ProductSearchContext ctx)
        {
            var query = PrepareProductSearchQuery(ctx, p => p.Id);
            return query.Distinct().Count();
        }

        public virtual IQueryable<TResult> PrepareProductSearchQuery<TResult>(
            ProductSearchContext ctx,
            Expression<Func<Product, TResult>> selector,
            IEnumerable<int> allowedCustomerRolesIds = null,
            bool searchLocalizedValue = false)
        {

            // products
            var query = ctx.Query ?? _productRepository.GetAll;
            query = query.Where(p => !p.Deleted);

            if (ctx.ProductIds != null && ctx.ProductIds.Count > 0)
            {
                query = query.Where(x => ctx.ProductIds.Contains(x.Id));
            }

            // category filtering
            if (ctx.CategoryIds != null && ctx.CategoryIds.Count > 0)
            {
                //search in subcategories
                if (ctx.MatchAllcategories)
                {
                    query = from p in query
                            where ctx.CategoryIds.All(i => p.ProductCategories.Any(p2 => p2.CategoryId == i))
                            from pc in p.ProductCategories
                            select p;
                }
                else
                {
                    query = from p in query
                            from pc in p.ProductCategories.Where(pc => ctx.CategoryIds.Contains(pc.CategoryId))
                            select p;
                }
            }

            if (ctx.BrandId > 0)
            {
                query = from p in query
                        where p.BrandId == ctx.BrandId
                        select p;
            }
            return query.Select(selector);
        }


        public IPagedList<Product> SearchProducts(ProductSearchContext ctx)
        {
            // validate "categoryIds"
            if (ctx.CategoryIds != null && ctx.CategoryIds.Contains(0))
                ctx.CategoryIds.Remove(0);

            var query = this.PrepareProductSearchQuery(ctx);

            query = from p in query
                    group p by p.Id into pGroup
                    orderby pGroup.Key
                    select pGroup.FirstOrDefault();

            //sort products
            if (ctx.CategoryIds != null && ctx.CategoryIds.Count > 0)
            {
                //category position
                var firstCategoryId = ctx.CategoryIds[0];
                query = query.OrderBy(p => p.ProductCategories.Where(pc => pc.CategoryId == firstCategoryId).FirstOrDefault());
            }
            else
            {
                query = query.OrderBy(p => p.Name);
            }

            var products = new PagedList<Product>(query, ctx.PageIndex, ctx.PageSize);

            return products;


        }

        public IQueryable<Product> PrepareProductSearchQuery(ProductSearchContext ctx, IEnumerable<int> allowedCustomerRolesIds = null, bool searchLocalizedValue = false)
        {
            return PrepareProductSearchQuery<Product>(ctx, x => x, allowedCustomerRolesIds, searchLocalizedValue);
        }

        public Product GetProductByBrandName(string brandName)
        {
            if (string.IsNullOrEmpty(brandName))
                return null;

            brandName = brandName.Trim();

            var product = _productRepository.GetAll
                .Where(x => !x.Deleted && x.Brand.Name == brandName)
                .OrderBy(x => x.Id)
                .FirstOrDefault();

            return product;
        }

        public Product GetProductByName(string name)
        {
            if (string.IsNullOrEmpty(name))
                return null;
            name = name.Trim();

            var product = _productRepository.GetAll
                .Where(x => !x.Deleted && x.Name == name)
                .OrderBy(x => x.Id)
                .FirstOrDefault();

            return product;
        }

        #endregion

        #region Product Picture

        public virtual void DeleteProductPicture(ProductPicture productPicture)
        {
            if (productPicture == null)
                throw new ArgumentNullException("productPicture");
            _productPictureRepository.Delete(productPicture);

        }

        public virtual IList<ProductPicture> GetProductPicturesByProductId(int productId)
        {
            var query = from pp in _productPictureRepository.GetAll
                        where pp.ProductId == productId
                        select pp;
            var productPictures = query.ToList();
            return productPictures;
        }

        public virtual ProductPicture GetProductPictureById(int productPictureId)
        {
            if (productPictureId == 0)
                return null;

            var pp = _productPictureRepository.GetById(productPictureId);
            return pp;
        }

        public virtual void InsertProductPicture(ProductPicture productPicture)
        {
            if (productPicture == null)
                throw new ArgumentNullException("productPicture");
            _productPictureRepository.Insert(productPicture);

        }

        public virtual void UpdateProductPicture(ProductPicture productPicture)
        {
            if (productPicture == null)
                throw new ArgumentNullException("productPicture");
            _productPictureRepository.Update(productPicture);
        }

        #endregion

        #endregion

    }
}
