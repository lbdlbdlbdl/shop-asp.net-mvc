﻿using Shop.Domain.Entities.Products;
using Shop.Domain.Entities.ShoppingCarts;
using Shop.Domain.Entities.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Services.ShoppingCarts
{
    public class AddToCartContext
    {
        public ShoppingCartItem Item { get; set; }
        public List<ShoppingCartItem> ChildItems { get; set; }

        public User User { get; set; }
        public Product Product { get; set; }
        public ShoppingCartType CartType { get; set; }
        public int Quantity { get; set; }
        
        }
    }
}
