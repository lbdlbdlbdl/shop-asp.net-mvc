﻿using Shop.Domain.Entities.ShoppingCarts;
using Shop.Domain.Entities.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Services.ShoppingCarts
{
    public static class ShoppingCartExtensions
    {
        public static int GetTotalProducts(this IEnumerable<ShoppingCartItem> shoppingCart)
        {
            return shoppingCart.Sum(x => x.Quantity);
        }

        public static User GetUser(this IList<ShoppingCartItem> shoppingCart)
        {
            if (shoppingCart.Count == 0)
                return null;
            return shoppingCart[0].User;
        }
    }
}
