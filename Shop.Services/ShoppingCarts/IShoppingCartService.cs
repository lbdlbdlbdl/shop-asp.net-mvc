﻿using Shop.Domain.Entities.Products;
using Shop.Domain.Entities.ShoppingCarts;
using Shop.Domain.Entities.Users;
using System.Collections.Generic;

namespace Shop.Services.ShoppingCarts
{
    public interface IShoppingCartService
    {
        void AddToCart(AddToCartContext ctx);

        ShoppingCartItem FindShoppingCartItemInTheCart(
            IList<ShoppingCartItem> shoppingCart,
            ShoppingCartType shoppingCartType,
            Product product);

        void AddToCart(User user, Product product, ShoppingCartType cartType, int quantity, AddToCartContext ctx = null);

        void DeleteShoppingCartItem(int shoppingCartItemId);
        void MigrateShoppingCart(User fromUser, User toUser);
        void DeleteShoppingCartItem(ShoppingCartItem shoppingCartItem);

        void UpdateShoppingCartItem(User user, int shoppingCartItemId, int newQuantity);
    }
}
