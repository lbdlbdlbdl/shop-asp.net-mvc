﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Domain.Entities.Products;
using Shop.Domain.Entities.ShoppingCarts;
using Shop.Domain.Entities.Users;
using Shop.Repository;
using Shop.Services.Users;
using Shop.Services.Products;
using System.Diagnostics;
using Shop.Core.Work;

namespace Shop.Services.ShoppingCarts
{
    public class ShoppingCartService : IShoppingCartService
    {
        #region Fields + Ctor

        private readonly IRepository<ShoppingCartItem> _sciRepository;
        private readonly IProductService _productService;
        private readonly IUserService _userService;
        private readonly IWorkContext _workContext;

        public ShoppingCartService(IRepository<ShoppingCartItem> sciRepository, IProductService productService, IUserService userService, IWorkContext workContext)
        {
            this._sciRepository = sciRepository;
            this._userService = userService;
            this._productService = productService;
            this._workContext = workContext;

        }

        #endregion

        public void AddToCart(AddToCartContext ctx)
        {
            var user = ctx.User ?? _workContext.CurrentUser;
            var cart = user.GetCartItems(ctx.CartType);

            AddToCartStoring(ctx);

        }

        /// <summary>
		/// Stores the shopping card items in the database
		/// </summary>
        public void AddToCartStoring(AddToCartContext ctx)
        {
            if (ctx.Item != null)
            {
                var user = ctx.User ?? _workContext.CurrentUser;
                user.ShoppingCartItems.Add(ctx.Item);
                _userService.UpdateUser(user);
            }
        }

        public virtual ShoppingCartItem FindShoppingCartItemInTheCart(
            IList<ShoppingCartItem> shoppingCart,
            ShoppingCartType shoppingCartType,
            Product product)
        {
            if (shoppingCart == null)
                throw new ArgumentNullException("shoppingCart");
            if (product == null)
                throw new ArgumentNullException("product");

            foreach (var sci in shoppingCart.Where(a => a.ShoppingCartType == shoppingCartType))
            {
                if (sci.ProductId == product.Id)
                {
                    return sci;
                }
            }
            return null;
        }

        public void AddToCart(User user, Product product, ShoppingCartType cartType, int quantity, AddToCartContext ctx = null)
        {
            if (user == null)
                throw new ArgumentNullException("customer");
            if (product == null)
                throw new ArgumentNullException("product");

            var cart = user.GetCartItems(cartType);
            ShoppingCartItem existingCartItem = null;

            existingCartItem = FindShoppingCartItemInTheCart(cart, cartType, product);

            if (existingCartItem != null)
            {
                //update existing shopping cart item
                int newQuantity = existingCartItem.Quantity + quantity;
                existingCartItem.Quantity = newQuantity;
                _userService.UpdateUser(user);
            }
            else // new shopping cart item
            {
                //maximum items validation
                if (cartType == ShoppingCartType.ShoppingCart && cart.Count >= 10)
                {
                    string error = "MaximumShoppingCartItems";
                }
                else if (cartType == ShoppingCartType.Wishlist && cart.Count >= 10)
                {
                    string error = "MaximumWishlistItems";
                }

                var now = DateTime.UtcNow;
                var cartItem = new ShoppingCartItem
                {
                    ShoppingCartType = cartType,
                    Product = product,
                    Quantity = quantity
                };

                if (ctx == null)
                {
                    user.ShoppingCartItems.Add(cartItem);
                    _userService.UpdateUser(user);
                }
                else
                {
                    Debug.Assert(ctx.Item == null, "Add to cart item already specified");
                    ctx.Item = cartItem;
                }
            }
        }

        public void DeleteShoppingCartItem(int shoppingCartItemId)
        {
            if (shoppingCartItemId != 0)
            {
                var shoppingCartItem = _sciRepository.GetById(shoppingCartItemId);
                DeleteShoppingCartItem(shoppingCartItem);
            }
        }

        public void DeleteShoppingCartItem(ShoppingCartItem shoppingCartItem)
        {
            if (shoppingCartItem == null)
                throw new ArgumentNullException("shoppingCartItem");

            int cartItemId = shoppingCartItem.Id;
            _sciRepository.Delete(shoppingCartItem);
        }

        public void UpdateShoppingCartItem(User user, int shoppingCartItemId, int newQuantity)
        {
            if (user == null)
                throw new ArgumentNullException("customer");
            var shoppingCartItem = user.ShoppingCartItems.FirstOrDefault(sci => sci.Id == shoppingCartItemId);
            if (shoppingCartItem != null)
            {
                if (newQuantity > 0)
                {
                    shoppingCartItem.Quantity = newQuantity;
                    _userService.UpdateUser(user);
                }
                else
                {
                    DeleteShoppingCartItem(shoppingCartItem);
                }
            }
        }

        public decimal GetShoppingCartSubTotal(IList<ShoppingCartItem> cart)
        {
            var subTotal = decimal.Zero;
            if (cart.Count == 0)
                return 0;
            foreach (var shoppingCartItem in cart)
            {
                if (shoppingCartItem.Product == null)
                    continue;
                decimal itemPrice = shoppingCartItem.Product.Price;
                subTotal = subTotal + decimal.Multiply(itemPrice, shoppingCartItem.Quantity);
            }
            return (subTotal < decimal.Zero ? decimal.Zero : subTotal);
        }

        public void MigrateShoppingCart(User fromUser, User toUser)
        {
            if (fromUser == null)
                throw new ArgumentNullException("fromCustomer");

            if (toUser == null)
                throw new ArgumentNullException("toCustomer");

            if (fromUser.Id == toUser.Id)
                return;
            
            var cartItems = fromUser.ShoppingCartItems.ToList();

            if (cartItems.Count <= 0)
                return;

            foreach (var cartItem in cartItems)
            {
                //TODO
                Copy(cartItem, toUser, cartItem.ShoppingCartType, false);
            }
            
            foreach (var cartItem in cartItems)
            {
                DeleteShoppingCartItem(cartItem);
            }
        }
    }
}
