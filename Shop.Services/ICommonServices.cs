﻿using Shop.Core.Work;

namespace Shop.Services
{
    public interface ICommonServices
    {
        IWorkContext WorkContext
        {
            get;
        }
    }
}
