﻿using System.Data.Entity;
using Shop.Domain.Entities;

namespace Shop.Domain
{
    public class ShopContext : DbContext
    {
        public DbSet<Address> Addresses { get; set; }

        public ShopContext() :
            base("name=ShopDbConnectionString")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Configure domain classes using modelBuilder here
            base.OnModelCreating(modelBuilder);
           // modelBuilder.Entity<Product>().Property(p => p.ProductImage).HasColumnName("image").HasColumnType("varbinary");
        }

    }
}
