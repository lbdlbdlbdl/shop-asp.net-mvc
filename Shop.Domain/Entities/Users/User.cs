﻿using Shop.Domain.Entities.Orders;
using Shop.Domain.Entities.ShoppingCarts;
using System;
using System.Collections.Generic;

namespace Shop.Domain.Entities.Users
{
    public class User : BaseEntity
    {
        public User()
        {
            this.UserRoles = new HashSet<UserRole>();
            this.ShoppingCartItems = new HashSet<ShoppingCartItem>();
            this.Orders = new HashSet<Order>();
        }

        public string Login { get; set; }
        public string Email { get; set; }
        public bool Active { get; set; }
        public bool IsRegistered { get; set; }
        public string Password { get; set; }
        public int PasswordStateId { get; set; }
        public string PasswordSalt { get; set; }
        public DateTime? LastLoginDateUtc { get; set; }
        public DateTime LastActivityDateUtc { get; set; }

        public virtual UserInfo UserInfo { get; set; }
        public virtual Address Address { get; set; }
        public PasswordState PasswordState
        {
            get { return (PasswordState)PasswordStateId; }
            set { this.PasswordStateId = (int)value; }

        }

        public virtual ICollection<UserRole> UserRoles { get; set; }
        public virtual ICollection<ShoppingCartItem> ShoppingCartItems { get; set; }
        public virtual ICollection<Order> Orders { get; set; }


    }
}
