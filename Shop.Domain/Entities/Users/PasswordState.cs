﻿namespace Shop.Domain.Entities.Users
{
    public enum PasswordState
    {
        Clear = 0,
        Hashed = 1
    }
}
