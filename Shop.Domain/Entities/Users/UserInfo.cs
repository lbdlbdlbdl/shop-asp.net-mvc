﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Shop.Domain.Entities.Users
{
    public class UserInfo : BaseEntity
    {
        [ForeignKey("User")]
        public override int Id {get;set;}
        public string FirstName { get { return "FirstName"; } }
        public string LastName { get { return "LastName"; } }
        public int AddressId { get; set; }
        public string PhoneNumber { get { return "PhoneNumber"; } }

        public User User { get; set; }
        public Address Address { get; set; }
    }
}
