﻿namespace Shop.Domain.Entities.ShoppingCarts
{
    public enum ShoppingCartType
    {
        ShoppingCart = 1,
        Wishlist = 2
    }
}
