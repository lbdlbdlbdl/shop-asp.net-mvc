﻿using Shop.Domain.Entities.Products;
using Shop.Domain.Entities.Users;

namespace Shop.Domain.Entities.ShoppingCarts
{
    public class ShoppingCartItem : BaseEntity
    {
        public int ShoppingCartTypeId { get; set; }
        public int UsertId { get; set;}
        public int ProductId { get; set; }
        public int Quantity { get; set; }

        public virtual ShoppingCartType ShoppingCartType { get; set; }
        public virtual User User { get; set; }
        public virtual Product Product { get; set; }
    }
}
