﻿namespace Shop.Domain.Entities.Media
{
    public class MediaStorage : BaseEntity
    {
        /// <summary>
        /// Binary data
        /// </summary>
        public byte[] Data { get; set; }
    }
}
