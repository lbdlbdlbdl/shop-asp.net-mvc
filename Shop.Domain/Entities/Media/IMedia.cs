﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Domain.Entities.Media
{
    public interface IMedia
    {
        int? MediaStorageId { get; set; }

        /// <summary>
        /// Gets or sets the media storage
        /// </summary>
        MediaStorage MediaStorage { get; set; }
    }
}
