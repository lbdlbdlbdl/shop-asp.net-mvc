﻿using Shop.Domain.Entities.Products;
using System;
using System.Collections.Generic;

namespace Shop.Domain.Entities.Media
{
    public class Picture : BaseEntity, IMedia
    {
        public Picture()
        {
            this.ProductPictures = new HashSet<ProductPicture>();
        }

        public byte[] PictureBinary;
        public string MimeType { get; set; }
        public int? MediaStorageId { get; set; }

        public virtual MediaStorage MediaStorage { get; set; }

        public virtual ICollection<ProductPicture> ProductPictures { get; set; }
    }
}
