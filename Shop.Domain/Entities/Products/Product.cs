﻿
using System.Collections.Generic;

namespace Shop.Domain.Entities.Products
{
    public class Product : BaseEntity
    {
        public Product()
        {
            this.ProductPictures = new HashSet<ProductPicture>();
            this.ProductCategories = new HashSet<ProductCategory>();
        }

        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }
        public int StockQuantity { get; set; }
        public decimal Price { get; set; }
        public bool Deleted { get; set; }

        public bool IsNew { get; set; }
        public int BrandId { get; set; }

        public virtual Brand Brand { get; set; }

        public virtual ICollection<ProductPicture> ProductPictures { get; set; }
        public virtual ICollection<ProductCategory> ProductCategories { get; set; }
    }
}
