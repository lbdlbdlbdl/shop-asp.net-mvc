﻿using Shop.Domain.Entities.Media;
using System.Collections.Generic;

namespace Shop.Domain.Entities.Products
{
    public class Category :BaseEntity
    {
        public Category()
        {
            this.Products = new HashSet<Product>();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public int ParentCategoryId { get; set; }
        public int? PictureId { get; set; }
        public bool Deleted { get; set; }

        public virtual Picture Picture { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
