﻿using Shop.Domain.Entities.Media;

namespace Shop.Domain.Entities.Products
{
    public class ProductPicture : BaseEntity
    {
        public int ProductId { get; set; }
        public int PictureId { get; set; }

        public virtual Product Product { get; set; }
        public virtual Picture Picture { get; set; }
    }
}
