﻿using Shop.Domain.Entities.Media;
using System.Collections.Generic;

namespace Shop.Domain.Entities.Products
{
    public class Brand : BaseEntity
    {
        public Brand()
        {
            this.Products = new HashSet<Product>();
        }
        public string Name { get; set; }
        public string Description { get; set; }
        public int PictureId { get; set; }

        public virtual Picture Picture { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
