﻿using Shop.Domain.Entities.Users;
using System;
using System.Collections.Generic;

namespace Shop.Domain.Entities.Orders
{
    public class Order : BaseEntity
    {
        public Order()
        {
            this.OrderItems = new HashSet<OrderItem>();
        }

        public string OrderNumber { get; set; }
        public int UserId { get; set; }
        public int OrderStatusId { get; set; }
        public int AddressId { get; set; }
        public string ShippingStatus{ get; set; }
        public decimal OrderShippingPrice { get; set; }
        public decimal OrderTotal { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public string Note { get; set; }
        public bool Deleted { get; set; }

        public virtual User User { get; set; }
        public virtual Address Address { get; set; }

        public virtual ICollection<OrderItem> OrderItems { get; set; }
    }
}
