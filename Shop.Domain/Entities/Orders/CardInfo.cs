﻿namespace Shop.Domain.Entities.Orders
{
    public class CardInfo : BaseEntity
    {
        public string CardType { get; set; }
        public string CardName { get; set; }
        public string CardNumber { get; set; }
        public string MaskedCreditCardNumber { get; set; }
        public string CardCvv { get; set; }
        public string CardExpirationMonth { get; set; }
        public string CardExpirationYear { get; set; }
    }
}
