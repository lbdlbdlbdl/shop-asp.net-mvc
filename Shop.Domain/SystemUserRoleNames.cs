﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Domain
{
    public static class SystemUserRoleNames
    {
        public static string Administrator { get { return "Administrator"; } }
        public static string Registered { get { return "Registered"; } }
        public static string Guest { get { return "Guest"; } }
    }
}
