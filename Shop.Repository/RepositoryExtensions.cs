﻿using Shop.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Repository
{
    public static class RepositoryExtensions
    {
        public static T GetFirst<T>(this IRepository<T> repository, Func<T, bool> predicate) where T : BaseEntity
        {
            return repository.GetAll.FirstOrDefault(predicate);
        }
    }
}
