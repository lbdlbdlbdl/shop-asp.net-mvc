﻿using Shop.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace Shop.Repository
{
    public interface IRepository<T> where T : BaseEntity
    {
        /// <summary>
        /// Returns the queryable entity set for the given type T.
        /// </summary>
        IQueryable<T> GetAll { get; }

        /// <summary>
        /// Creates a new instance of an entity of type T
        /// </summary>
        /// <returns>The new entity instance.</returns>
        T Create();

        /// <summary>
        /// Gets an entity by id from the database or the local change tracker.
        /// </summary>
        /// <param name="id">The id of the entity. This can also be a composite key.</param>
        /// <returns>The resolved entity</returns>
        T GetById(object id);

        void Insert(T entity);

        /// <summary>
        /// Marks the changes of an existing entity to be saved to the store.
        /// </summary>
        void Update(T entity);

        void Delete(T entity);
    }
}
