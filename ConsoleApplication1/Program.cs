﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Shop.Domain;
using Shop.Domain.Entities;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new ShopContext())
            {
                var prd = new Product();
                db.Products.Add(prd);
                db.SaveChanges();
            }
        }
    }
}
